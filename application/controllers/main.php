<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Main extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url');
            $this->load->helper('html');
            $this->load->helper('h');
            $this->load->database();
            $this->load->model('user');                    
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $this->load->library('traduccion');
            $this->load->model('querys');     
            $this->load->model('bdsource');
            $this->bdsource->init('ajustes',true);
            $banner = new Bdsource('banner');
            $banner->order_by = array('priority','ASC');
            $banner->init('',FALSE,'',TRUE);
            ini_set('show_errors',1);
            ini_set('display_errors',1);
            
            if(empty($_SESSION['lang'])){
                $_SESSION['lang'] = 'es';
            }
        }

        public function index()
        {
            $page = new Bdsource('paginas');
            $page->where('titulo','home');
            $page->init();
            $clientes = new Bdsource('clientes');
            $clientes->order_by = array('priority','ASC');
            $clientes->init();
            
            $proyectos = new Bdsource('proyectos');
            $proyectos->limit = array('4');            
            $proyectos->init();
            
            foreach($proyectos->result() as $n=>$c){
                $proyectos->result->row($n)->galeria = $this->db->get_where('proyectos_fotos',array('proyectos_id'=>$c->id));
            }
            
            $data = array(
                'view'=>'main',
                'page'=>$page->texto,
                'clientes'=>$clientes,
                'proyectos'=>$proyectos
            );
            $this->loadView($data);                
        }                

        public function success($msj)
        {
                return '<div class="alert alert-success">'.$msj.'</div>';
        }

        public function error($msj)
        {
                return '<div class="alert alert-danger">'.$msj.'</div>';
        }

        public function login()
        {
                if(!$this->user->log)
                {	
                        if(!empty($_POST['email']) && !empty($_POST['pass']))
                        {
                                $this->db->where('email',$this->input->post('email'));
                                $r = $this->db->get('user');
                                if($this->user->login($this->input->post('email',TRUE),$this->input->post('pass',TRUE)))
                                {
                                        if($r->num_rows>0 && $r->row()->status==1)
                                        {
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('main').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
                                        }
                                        else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                                }
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                        }
                        else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url('registro/index/add'));
                }
                else header("Location:".base_url('registro/index/add'));
        }       

        public function unlog()
        {
                $this->user->unlog();                
                header("Location:".site_url());
        }

        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param)){
                $param = array('view'=>$param);
            }
            $view = $this->load->view('template',$param,TRUE);            
            echo $this->traduccion->traducir($view,$_SESSION['lang']);
            
        }
        
        public function traducir($idioma = 'ca'){
            $_SESSION['lang'] = $idioma;
            header("Location:".$_SERVER['HTTP_REFERER']);
        }

        public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }                

         function error404(){
            $this->loadView(array('view'=>'errors/403'));
        }    
        
        function contacto(){
            if(empty($_POST)){
                $this->loadView(array('view'=>'contacto','title'=>'Contacto'));
            }else{
                $mensaje = '<h1>Hola, te han enviado un mensaje desde <a href="http://www.navamiranda.com.ve">www.navamiranda.com.ve</a></h1>';
                $mensaje.= '<h2>Datos</h2>';
                foreach($_POST as $n=>$p){
                    $mensaje.= '<p><b>'.ucfirst($n).'</b> = '.$p.'</p>';
                }
                correo($this->ajustes->correo,'Solicitud de contacto',$mensaje);
            }
        }
        
        function propiedades(){
            $this->loadView('propiedad');
        }
        
        function detail(){
            $this->loadView('detail');
        }
}
