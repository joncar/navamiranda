<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
            <?php if($this->user->admin==1): ?>
            <!--- Admin --->
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-lock"></i>
                        <span class="menu-text">Admin</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li>
                        <a href="<?= base_url('admin/ajustes') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Ajustes</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/paginas') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Paginas</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/banner') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Banner</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/clientes') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Clientes</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/proyectos') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Proyectos</span>                                    
                        </a>                                
                    </li>
                    <!--- Comidas ---->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Productos</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('foods/admin/gamas') ?>">
                                    <i class="menu-icon fa fa-users"></i>
                                    <span class="menu-text">Categorías</span>                                    
                                </a>                                
                            </li>                            
                            <li>
                                <a href="<?= base_url('foods/admin/foods') ?>">
                                    <i class="menu-icon fa fa-cutlery"></i>
                                    <span class="menu-text">Productos</span>                                    
                                </a>                                
                            </li>
                        </ul>
                    </li>                                        
                    <!--- Seguridad --->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Seguridad</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('seguridad/grupos') ?>">
                                    <i class="menu-icon fa fa-users"></i>
                                    <span class="menu-text">Grupos</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/funciones') ?>">
                                    <i class="menu-icon fa fa-arrows"></i>
                                    <span class="menu-text">Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/user') ?>">
                                    <i class="menu-icon fa fa-user"></i>
                                    <span class="menu-text">Usuarios</span>                                    
                                </a>                                
                            </li>
                        </ul>
                    </li>
                    <!--- Fin seguridad ---->
                </ul>
            </li>
            <!--- Fin Admin ---->
            <?php endif ?>
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed');
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
