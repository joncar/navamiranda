<nav class="navbar navbar-default menuontop" id="menu">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?= site_url() ?>">INICIO</a></li>
                <li><a href="<?= site_url() ?>#empresa">EMPRESA</a></li>
                <li><a href="<?= site_url() ?>#servicios">SERVICIOS</a></li>
                <li><a href="<?= site_url('productos') ?>">PRODUCTOS</a></li>
                <li><a href="<?= site_url() ?>#clientes">CLIENTES</a></li>
                <li><a href="<?= site_url() ?>#proyectos">PROYECTOS</a></li>
                <li><a href="<?= site_url() ?>#contacto">CONTACTO</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>