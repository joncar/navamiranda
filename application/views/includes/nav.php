<div class="nav-background">
        <div class="container">
            <div class="clearfix navbar">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <div class="nav-collapse collapse">
                            <nav id="nav-main" role="navigation">
                                <ul id="menu-primary-menu" class="nav">
                                    <li class="current_page_item"><a href="<?= site_url() ?>">Inicio</a></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="<?= site_url('propiedad/lista') ?>?categorias_id=1">Solars</a>
                                            <ul class="dropdown-menu">
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=1&tipo_venta=2">Lloguer</a></li>
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=1&tipo_venta=1">Venda</a></li>
                                            </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="<?= site_url('propiedad/lista') ?>?categorias_id=2">Naus Industrials</a>
                                            <ul class="dropdown-menu">
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=2&tipo_venta=2">Lloguer</a></li>
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=2&tipo_venta=1">Venda</a></li>
                                            </ul>
                                    </li>
                                    <li><a href="<?= site_url('propiedad/listado') ?>">Llistat</a></li>
                                    <li><a href="<?= site_url('actualitat') ?>">Actualitat</a></li>
                                    <li><a href="<?= site_url('contacte') ?>">Contacte</a></li>
                                </ul>
                                <div id="social-network">
                                    <a class="fb" href="<?= $this->ajustes->facebook ?>" title="Facebook"><i class="fa fa-facebook"></i></a>
                                    <a class="tw" href="<?= $this->ajustes->twitter ?>" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    <a class="rss" href="<?= $this->ajustes->instagram ?>" title="Instagram"><i class="fa fa-instagram"></i></a>					
                                    <a class="gp" href="<?= $this->ajustes->google ?>" title="Google Plus"><i class="fa fa-google-plus"></i></a>	
                                </div>
                            </nav> <!-- #nav-main -->
                        </div>
                    </div>
                </div>
            </div><!-- .navbar -->
        </div>
    </div>