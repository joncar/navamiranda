<header class="container-fluid main">
    <?php foreach($this->banner->result() as $n=>$b): ?>
        <div class="banner <?= $n==0?'active':'' ?>" style="background:url(<?= base_url().'uploads/banner/'.$b->foto ?>) no-repeat; background-size:cover;"></div>
    <?php endforeach ?>
    <?= $this->load->view('includes/menu') ?>
    <div class="social">
        <a href="<?= $this->ajustes->facebook ?>"><i class="fa fa-facebook"></i></a>
        <a href="<?= $this->ajustes->twitter ?>"><i class="fa fa-twitter"></i></a>
        <a href="<?= $this->ajustes->linkedin ?>"><i class="fa fa-linkedin"></i></a>
        <a href="<?= $this->ajustes->google ?>"><i class="fa fa-google-plus"></i></a>
        <a href="<?= $this->ajustes->instagram ?>"><i class="fa fa-instagram"></i></a>
    </div>
</header>
<script>
    var cantidad = $(".banner").length-1;
    var index = 1;
    var timetochange = 5000;
    $(document).ready(function(){        
        setTimeout(changeBanner,timetochange);
        $(window).on('scroll',function(){
            if($(this).scrollTop()>0)
                $("#menu").addClass('menuonbottom').removeClass('menuontop');
            else
                $("#menu").addClass('menuontop').removeClass('menuonbottom');
        });
        $(window).trigger('scroll');
    });
    
    function changeBanner(){
        $(".banner").removeClass('active');
        var b = $("header").find('.banner')[index];
        $(b).addClass('active');
        index = index<cantidad?index+1:0;
        setTimeout(changeBanner,timetochange);
    }
</script>