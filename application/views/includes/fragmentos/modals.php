<div class="modal fade" id="signin" role="dialog" aria-labelledby="signinLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="signinLabel">Conéctate</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="userLoginForm" method="post">
                    <div class="signinMessage" id="signinMessage"></div>                                    
                    <div class="form-group">
                        <div class="btn-group-justified">
                            <a href="#" class="btn btn-lg btn-facebook" id="fbLoginBtn">
                                <span class="fa fa-facebook pull-left"></span>
                                <span class="signinFBText">Conectate en Facebook</span>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="btn-group-justified">
                            <a href="#" class="btn btn-lg btn-google" id="googleSigninBtn">
                                <span class="fa fa-google-plus pull-left"></span>
                                <span class="signinGText">Conectate con Google</span>
                            </a>
                        </div>
                    </div>
                    <div class="signOr">O</div>
                    <div class="form-group">
                        <input type="text" name="usernameSignin" id="usernameSignin" placeholder="Username" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="passwordSignin" id="passwordSignin" placeholder="Password" class="form-control">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="checkbox custom-checkbox">
                                    <label>
                                        <input type="checkbox" id="rememberSignin" name="rememberme" value="forever">
                                        <span class="fa fa-check"></span> Recuerdame
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-6 align-right">
                                <p class="help-block"><a href="#" class="text-green forgotPass">Olvidastes tu contraseña?</a></p>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="securitySignin" name="securitySignin" value="d3a5246d5c" />
                    <input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/" />                    
                    <div class="form-group">
                        <div class="btn-group-justified">
                            <a href="#" class="btn btn-lg btn-green" id="submitSignin">Conectate</a>
                        </div>
                    </div>
                    <p class="help-block">Don't have an account? <a href="#" class="modal-su text-green">Registrate</a></p>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="signup" role="dialog" aria-labelledby="signupLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="signupLabel">Registrate</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="userSignupForm" method="post">
                    <div class="signinMessage" id="signupMessage"></div>
                    <div class="form-group">
                        <div class="checkbox custom-checkbox">
                            <label>
                                <input type="checkbox" id="register_as_agent" name="register_as_agent" value="1">
                                <span class="fa fa-check"></span> Registrarme como un agente</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="usernameSignup" id="usernameSignup" placeholder="Username" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" name="firstnameSignup" id="firstnameSignup" placeholder="First Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" name="lastnameSignup" id="lastnameSignup" placeholder="Last Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" name="emailSignup" id="emailSignup" placeholder="Email Address" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="pass1Signup" id="pass1Signup" placeholder="Password" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="pass2Signup" id="pass2Signup" placeholder="Confirm Password" class="form-control">
                    </div>
                    <div class="form-group">
                        <p class="help-block"><a href="#" class="text-green forgotPass">Olvidastes tu contraseña?</a></p>
                    </div>
                    <input type="hidden" id="securitySignup" name="securitySignup" value="dea170a75d" />
                    <input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/" />                    
                    <div class="form-group">
                        <div class="btn-group-justified">
                            <a class="btn btn-lg btn-green" id="submitSignup">Conectate</a>
                        </div>
                    </div>
                    <p class="help-block">Ya eres un miembro? 
                        <a href="#" class="modal-si text-green">Registrate</a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="forgot" role="dialog" aria-labelledby="forgotLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="forgotLabel">¿Olvidastes tu contraseña?</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="userForgotPassForm" method="post">
                    <div class="forgotMessage" id="forgotMessage"></div>
                    <div class="form-group forgotField">
                        <input type="text" name="emailForgot" id="emailForgot" placeholder="Username or Email address" class="form-control">
                    </div>
                    <input type="hidden" id="securityForgot" name="securityForgot" value="3c5d7c3beb" /><input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/" />                    <div class="form-group forgotField">
                        <div class="btn-group-justified">
                            <a href="#" class="btn btn-lg btn-green" id="submitForgot">Consigue un nuevo password</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resetpass" role="dialog" aria-labelledby="resetpassLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="resetpassLabel">Resetear Password</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="userResetPassForm" method="post">
                    <div class="resetPassMessage" id="resetPassMessage"></div>
                    <div class="form-group">
                        <input type="password" name="resetPass_1" id="resetPass_1" placeholder="New Password" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="resetPass_2" id="resetPass_2" placeholder="Confirm Password" class="form-control">
                    </div>
                    <p class="help-block">Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).</p>
                    <input type="hidden" id="securityResetpass" name="securityResetpass" value="027bc48fcb" /><input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/" />                    <div class="form-group">
                        <div class="btn-group-justified">
                            <a href="#" class="btn btn-lg btn-green" id="submitResetPass">Reset Password</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="searches-modal" role="dialog" aria-labelledby="searches-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-close"></span></button>
                <h4 class="modal-title" id="searches-label">Mis búsquedas</h4>
            </div>
            <div class="modal-body"></div>
            <input type="hidden" name="modal-user-id" id="modal-user-id" value="0">
            <input type="hidden" id="securityDeleteSearch" name="securityDeleteSearch" value="c7c8fbceae" /><input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/" />            <div class="modal-footer">
                <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-gray">Cerrar</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="save-search-modal" role="dialog" aria-labelledby="save-search-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-close"></span></button>
                <h4 class="modal-title" id="save-search-label">Save Search</h4>
            </div>
            <div class="modal-body">
                <form id="save-search-form">
                    <div class="save-search-message" id="save-search-message"></div>
                    <div class="form-group">
                        <label for="save-search-name">Name</label>
                        <input type="text" id="save-search-name" name="save-search-name" placeholder="Enter a name for your search" class="form-control">
                                                <input type="hidden" id="save-search-user" name="save-search-user" value="0">
                    </div>
                    <input type="hidden" id="securitySaveSearch" name="securitySaveSearch" value="104138ff8f" /><input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/search-results/?search_city=San+Francisco&amp;search_lat=37.7749295&amp;search_lng=-122.41941550000001&amp;search_category=0&amp;search_type=0&amp;search_min_price=&amp;search_max_price=" />                </form>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-gray">Cancel</a>
                <a href="javascript:void(0);" class="btn btn-green" id="save-search-btn">Save</a>
            </div>
        </div>
    </div>
</div>