<div class="topUserWraper">
    <a href="#" class="userNavHandler">
        <span class="icon-user"></span>
    </a>
    <div class="user-nav">
        <ul>
            <li><a href="#" data-toggle="modal" data-target="#signup">Sign Up</a></li>
            <li><a href="#" data-toggle="modal" data-target="#signin">Sign In</a></li>
        </ul>
    </div>
</div>

<?php $this->load->view('includes/fragmentos/modals'); ?>
<a href="javascript:void(0);" class="top-navHandler visible-xs">
    <span class="fa fa-bars"></span>
</a>
<div class="top-nav">
    <div class="menu-top-menu-container">
        <ul class="menu">
            <li>
                <a href="<?= site_url() ?>">Inicio</a>
            </li>
            <li>
                <a href="#">
                    Propiedades <span class="fa fa-angle-down"></span>
                </a>
                <ul class="sub-menu">
                    <li><a href="#">Properties List</a></li>
                    <li><a href="#">Single Property</a></li>
                    <li><a href="#">Search by City</a></li>
                    <li><a href="#">Search by Category</a></li>
                    <li><a href="#">Search by Type</a></li>
                    <li><a href="#">Search by Price</a></li>
                </ul>
            </li>
            <li><a href="#">IDX <span class="fa fa-angle-down"></span></a>
                <ul class="sub-menu">
                    <li><a href="#">dsIDXpress Listings</a></li>
                    <li><a href="#">dsIDXpress Single Listing Shortcode</a></li>
                    <li><a href="#">dsIDXpress Listings Shortcode</a></li>
                </ul>
            </li>
            <li><a href="#">Agents <span class="fa fa-angle-down"></span></a>
                <ul class="sub-menu">
                    <li><a href="#">Agents List</a></li>
                    <li><a href="#">Agent Page</a></li>
                </ul>
            </li>
            <li><a href="#">Customers</a></li>            
            <li><a href="#">Features <span class="fa fa-angle-down"></span></a>
                <ul class="sub-menu">
                    <li><a href="#">Front-end Property Submission</a></li>
                    <li><a href="#">Custom Widgets</a></li>
                    <li><a href="#">Reales WP Shortcodes</a></li>
                </ul>
            </li>
            <li><a href="#">Contact Us</a></li>
        </ul>
    </div>
</div>