<div class="home-footer">
    <div class="page-wrapper">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <ul class="footer-nav pb20">
                    <li class="widget-container widget_nav_menu">
                        <h3 class="osLight footer-header">Gamas más visitadas</h3>
                        <div class="menu-footer-menu-1-container">
                            <ul class="menu">
                                <li><a href="http://mariusn.com/themes/reales-wp/search-results/?search_city=San+Francisco&#038;search_lat=37.7749295&#038;search_lng=-122.41941550000001&#038;search_category=0&#038;search_type=0&#038;search_min_price=&#038;search_max_price=">Properties List</a></li>
                                <li><a href="http://mariusn.com/themes/reales-wp/properties/elegant-apartment/">Single Property</a></li>
                                <li><a href="http://mariusn.com/themes/reales-wp/properties/elegant-apartment/">Search by City: San Francisco</a></li>
                                <li><a href="http://mariusn.com/themes/reales-wp/search-results/?search_city=San+Francisco&#038;search_lat=37.7749295&#038;search_lng=-122.41941550000001&#038;search_category=29&#038;search_type=0&#038;search_min_price=&#038;search_max_price=">Search by Category: Apartment</a></li>
                                <li><a href="http://mariusn.com/themes/reales-wp/search-results/?search_city=San+Francisco&#038;search_lat=37.7749295&#038;search_lng=-122.41941550000001&#038;search_category=0&#038;search_type=10&#038;search_min_price=&#038;search_max_price=">Search by Type: For Rent</a></li>
                                <li><a href="http://mariusn.com/themes/reales-wp/search-results/?search_city=San+Francisco&#038;search_lat=37.7749295&#038;search_lng=-122.41941550000001&#038;search_category=0&#038;search_type=0&#038;search_min_price=&#038;search_max_price=700000">Search by Price: Less than $70,000</a></li>
                                <li><a href="http://mariusn.com/themes/reales-wp/search-results/?search_city=San+Francisco&#038;search_lat=37.7749295&#038;search_lng=-122.41941550000001&#038;search_category=0&#038;search_type=0&#038;search_min_price=&#038;search_max_price=700000">Search by Price: Less than $70,000</a></li>
                            </ul>
                        </div>
                    </li>                    
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <ul class="footer-nav pb20">
                    <li class="widget-container recent_properties_sidebar">
                        <h3 class="osLight footer-header">Aplicaciones más visitadas</h3>
                        <div class="propsWidget"><ul class="propList"><li>
                                    <a href="http://mariusn.com/themes/reales-wp/properties/modern-residence/">
                                        <div class="image">
                                            <img src="http://mariusn.com/themes/reales-wp/wp-content/uploads/2014/12/bg-1.jpg" alt="Recently Listed Properties" />
                                        </div>
                                        <div class="info text-nowrap">
                                            <div class="name">Modern Residence</div>
                                            <div class="address">547 35th Ave, San Francisco, 94121, United States</div>
                                            <div class="price">
                                                $23,234 
                                                <span class="badge">For Sale</span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://mariusn.com/themes/reales-wp/properties/sophisticated-residence/">
                                        <div class="image">
                                            <div class="featured-label">
                                                <div class="featured-label-left"></div>
                                                <div class="featured-label-content">
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="featured-label-right"></div>
                                                <div class="clearfix"></div>                                                    
                                            </div>
                                            <img src="http://mariusn.com/themes/reales-wp/wp-content/uploads/2015/02/img-prop.jpg" alt="Recently Listed Properties" />
                                        </div>
                                        <div class="info text-nowrap">
                                            <div class="name">Sophisticated Residence</div>
                                            <div class="address">600 40th Ave, San Francisco, 94121, United States</div>
                                            <div class="price">$799,000 <span class="badge">For Sale</span></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://mariusn.com/themes/reales-wp/properties/luxury-mansion/">
                                        <div class="image">
                                            <div class="featured-label">
                                                <div class="featured-label-left"></div>
                                                <div class="featured-label-content">
                                                    <span class="fa fa-star"></span>
                                                </div>
                                                <div class="featured-label-right"></div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <img src="http://mariusn.com/themes/reales-wp/wp-content/uploads/2014/12/bg-5-1024x576.jpg" alt="Recently Listed Properties" />
                                        </div>
                                        <div class="info text-nowrap">
                                            <div class="name">Luxury Mansion</div>
                                            <div class="address">10 Romain St, San Francisco, 123456, Romania</div>
                                            <div class="price">
                                                $3,400/mo <span class="badge">For Rent</span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <ul class="footer-nav pb20">
                    <li class="widget-container widget_recent_entries">
                        <h3 class="osLight footer-header">Ubicación</h3>
                        <img src="http://maps.google.com/maps/api/staticmap?center=0,0&zoom=17&markers=size:medium|color:red|0,0&path=color:0x0000FF80|weight:2|0,0&size=480x360&sensor=false" style='width: 100%'/>
                    </li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <ul class="footer-nav pb20">
                    <li class="widget-container contact_sidebar">
                        <h3 class="osLight footer-header">Oficinas</h3>
                        <ul>
                            <li class="widget-phone"><span class="fa fa-phone"></span> (123) 456-7890</li>
                            <li class="widget-address osLight"><p>196 Front St</p><p>San Francisco, CA 94111</p><p>United States</p></li>
                        </ul>
                        </li>
                        <li class="widget-container social_sidebar">
                            <h3 class="osLight footer-header">Siguenos</h3>
                            <ul>
                                <li>
                                    <a href="#facebook" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-facebook"></span>
                                    </a>
                                    <a href="#twitter" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-twitter"></span>
                                    </a> <a href="#google" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-google-plus"></span>
                                    </a> 
                                    <a href="#linkedin" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-linkedin"></span>
                                    </a> 
                                </li>
                            </ul>
                        </li>
                </ul>
            </div>
        </div>
        <div class="copyright">© 2015 Reales WP - Real Estate WordPress Theme</div>
    </div>
</div>