<?php $this->load->view('includes/headerMain'); ?>
<section class="container-fluid gris" id="empresa">
    <h1><span>Empresa</span></h1>
    <?= $this->db->get_where('paginas',array('titulo'=>'Empresa'))->row()->texto ?>
</section>
<section class="container-fluid rojo" id="servicios">
    <h1><span>Servicios</span></h1>
    <?= $this->db->get_where('paginas',array('titulo'=>'Servicios'))->row()->texto ?>
</section>
<section class="container-fluid blanco" id="productos" style="text-align: justify;">
    <h1><span>Productos</span></h1>
    <?= $this->db->get_where('paginas',array('titulo'=>'Productos'))->row()->texto ?>
</section>
<section class="container-fluid gris" id="proyectos" style="text-align: justify;">
    <h1><span>Proyectos</span></h1>
    <?php foreach($proyectos->result() as $c): ?>
        <div class="col-xs-12 col-sm-3">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <?php foreach($this->db->get_where('proyectos_fotos',array('proyectos_id'=>$c->id))->result() as $n=>$g): ?>
                    <div class="item <?= $n==0?'active':'' ?>" style="background:url(<?= base_url('img/proyectos/'.$g->foto) ?>) no-repeat; width:100%; height:180px; background-size:cover; background-position: center;"></div>
                  <?php endforeach ?>
                </div>
            </div>
            <p><?= $c->descripcion ?></p>
        </div>
    <?php endforeach ?>
</section>
<section class="container-fluid rojo" id="clientes" style="text-align: justify;">
    <h1><span>Clientes</span></h1>
    <div class="row" style="margin-left: 0px; margin-right: 0px;">
        <?php foreach($clientes->result() as $c): ?>
        <a href="<?= $c->enlace ?>" class="cliente"><div class="col-xs-4 col-sm-1" style="background:url(<?= base_url('img/clientes/'.$c->foto) ?>) no-repeat; height:80px; border-radius:100%; background-size:cover; background-position:center;"></div></a>
        <?php endforeach ?>
    </div>
</section>
<section class="container-fluid blanco" id="contacto" style="text-align: justify;">
    <h1><span>Contacto</span></h1>
    <div class="row" style="margin-left: 0px; margin-right: 0px;">
        <div class="col-xs-12 col-sm-6">
            <h3><?= $this->ajustes->titulo_contacto ?></h3>
            <p><?= $this->ajustes->texto_contacto ?></p>
            <p><?= $this->ajustes->telefono ?></p>
            <p><?= $this->ajustes->correo ?></p>
            <p><?= $this->ajustes->direccion_contacto ?></p>
            <div id="mapa" style="width:100%; height:200px;"></div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <form id="formcontact" class="form-horizontal" onsubmit="return sendMessage()">
                    <div class="form-group">
                      <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                      </div>
                    </div>
                   <div class="form-group">
                      <label for="telefono" class="col-sm-2 control-label">Teléfono</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono">
                      </div>
                    </div>
                  <div class="form-group">
                      <label for="email" class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                      </div>
                    </div>
                    <div class="form-group">                      
                      <div class="col-sm-10 col-sm-offset-2">
                          <textarea class="form-control" name="mensaje" placeholder="Mensaje"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success btn-block">Enviar mensaje</button>
                      </div>
                    </div>
                  </form>
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $("a[href*=#]").click(function(e){
            e.preventDefault();
            var target = $(this).attr('href');
            target = target.split("#");
            target = $("#"+target[1]);            
            $('html, body').stop().animate({'scrollTop': parseInt(target.offset().top)-30}, 900, 'swing');        
        });
        
    });
    
    function sendMessage(){
        var datos = document.getElementById('formcontact');
        var f = new FormData(datos);
        console.log(f);
        $.ajax({
            url:'<?= base_url('main/contacto') ?>',
            data:f,
            cache: false,
            contentType: false,
            processData: false,
            type:'POST',
            success:function(data){
                alert('Su mensaje ha sido enviado con éxito');
            }
        });
        return false;
    }
    
    <?php 
        $map = maptoarray($this->ajustes->mapa_contacto);
        echo 'var lat = "'.$map[0].'", lon = "'.$map[1].'"; ';
    ?>
    var mapOptions = {
        zoom: 9,
        center: new google.maps.LatLng(lat,lon)
    };   
    map = new google.maps.Map(document.getElementById('mapa'), mapOptions);   
    new google.maps.Marker({ position: new google.maps.LatLng(lat,lon), map: map, title: 'Nuestra ubicación' });
</script>