<!DOCTYPE html>
<html>
    <head>
        <title><?= empty($title)?'Navamiranda':$title ?></title>
        <meta charset="utf8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url('css/style.css') ?>">
        <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?libraries=geometry&#038;libraries=places&#038;ver=1.0'></script>
    </head>
    <body>        
        <?php $this->load->view($view); ?>
    </body>
</html>
