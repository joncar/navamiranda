<section class="container-fluid blanco">
        <div class="col-xs-12 col-sm-4 col-sm-offset-1">
            <div class="form-container">
                <?= $this->load->view('predesign/login') ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-sm-offset-1">
            <div class="form-container">
                <?= $output ?>
            </div>
        </div>            
</section>