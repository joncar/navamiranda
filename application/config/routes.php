<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "main";
$route['contacte'] = "main/contacto";
$route['productos'] = "foods/front/lista";
$route['productos/(:any)-(:any)'] = "foods/front/read/$1";
$route['productos/(:any)'] = "foods/front/$1";
$route['beverage'] = "beverages/front";
$route['404_override'] = 'main/error404';
$route['403_override'] = 'main/error403';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
