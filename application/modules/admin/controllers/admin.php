<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function ajustes(){
            $crud = $this->crud_function('','');
            $crud->field_type('mapa_contacto','map',array('width'=>'300px','height'=>'300px'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function categorias(){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function agentes(){
            $crud = $this->crud_function('','');            
            $crud->set_field_upload('agente_foto','uploads');
            $crud->required_fields('nombre','correo','telefono','agente_foto');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function paginas(){
            $crud = $this->crud_function('','');
            $crud->required_fields('titulo','texto');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function clonarPropiedad($id){
            if(is_numeric($id)){
                $propiedad = new Bdsource();
                $propiedad->where('id',$id);
                $propiedad->init('propiedades',TRUE);
                $data = (array)$this->propiedades;                
                $propiedad->save($data,'',TRUE);
                header("Location:".base_url('admin/propiedades/edit/'.$propiedad->getid()));
            }
        }
        
        public function propiedades(){
            $crud = $this->crud_function('','');
            $crud->display_as('categorias_id','Categoria')
                     ->display_as('agentes_id','Agente')
                     ->display_as('descripcion','Descripción')
                     ->display_as('precio','Precio de venta')
                     ->field_type('tipo_venta','dropdown',array('1'=>'Venta','2'=>'Alquiler'));
            $crud->field_type('ubicacion','map',array('width'=>'300px','height'=>'300px'))
                     ->field_type('idioma','dropdown',array("ca"=>"Catalán","es"=>"Español"))
                     ->set_field_upload('foto_portada','uploads');
            $crud->add_action('<i class="fa fa-image"></i> Adm. Fotos','',base_url('admin/fotos').'/');
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('admin/clonarPropiedad').'/');
            $crud->display_as('comarcas_id','Comarca')
                     ->display_as('categorias_id','Categoria')
                     ->display_as('agentes_id','Agente')
                     ->display_as('nombre_propiedad','Propiedad');
            $crud->columns('comarcas_id','categorias_id','agentes_id','nombre_propiedad','tipo_venta','precio','idioma');
            $crud = $crud->render();
            $crud->crud = 'propiedades';
            $crud->title = 'Registro de propiedades';
            $this->loadView($crud);
        }
        
        public function proyectos(){
            $this->load->library('image_crud');
            $crud = $this->crud_function('','');      
            $crud->add_action('<i class="fa fa-photo"></i> Añadir Fotos','',base_url('admin/proyectos_fotos/').'/');
            $crud = $crud->render();
            $crud->crud = 'proyectos';
            $this->loadView($crud);            
        }
        
        public function fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('fotos')                        
                ->set_image_path('uploads')
                ->set_relation_field('propiedades_id')
                ->set_ordering_field('priority')
                ->set_url_field('foto')
                ->module = 'admin';
            $this->loadView($crud->render());
        }
        
        public function banner(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('banner')                        
                ->set_image_path('uploads/banner')                
                ->set_ordering_field('priority')                
                ->set_url_field('foto')
                ->module = 'admin';
            $this->loadView($crud->render());
        }
        
        public function clientes(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('clientes')                        
                ->set_image_path('img/clientes')
                ->set_title_field('enlace')
                ->set_ordering_field('priority')
                ->set_url_field('foto')
                ->module = 'admin';
            $this->loadView($crud->render());
        }
        
        public function proyectos_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('proyectos_fotos')                        
                     ->set_image_path('img/proyectos')                
                     ->set_ordering_field('priority')
                     ->set_relation_field('proyectos_id')
                     ->set_url_field('foto')
                     ->module = 'admin';            
            $this->loadView($crud->render());
        }
    }
?>
