<div class="filter">
    <h3><span class="fa fa-search"></span> Búsqueda</h3>       
    <form class="filterForm" id="filterPropertyForm" role="search" method="get" action="<?= base_url('productos/lista') ?>">        
        <div class="row">
            <div class="col-xs-12">
                    <div class="form-group">
                       <label>Búsqueda</label>                        
                       <div><input class="form-control" name="descripcion" id="descripcion" value="<?= !empty($_GET['descripcion'])?$_GET['descripcion']:'' ?>" placeholder="Introduzca sus parámetros de búsqueda" autocomplete="off" type="text"></div>
                       <div>                           
                           <?= form_dropdown_from_query('gamas_id',$gamas->result,'id','gamas_nombre',!empty($_GET['gamas_id'])?$_GET['gamas_id']:0,'',TRUE,'','Categorías') ?>
                       </div>
                    </div>
                </div>                
                <div class="col-xs-12">
                    <div class="form-group">                        
                        <input type='hidden' name='page' id='page' value='<?= empty($_GET['page'])?1:$_GET['page'] ?>'>
                        <input type='hidden' name='order_by' id='order_by' value='<?= empty($_GET['order_by'])?'foods.id_ASC':$_GET['order_by'] ?>'>
                        <button type="submit" class="btn btn-success btn-block" id="filterPropertySubmit">Buscar</button>
                        <!--<a href="javascript:void(0);" class="btn btn-gray display mb-10" id="showAdvancedFilter">Búsqueda avanzada</a>
                        <a href="javascript:void(0);" class="btn btn-gray mb-10" id="hideAdvancedFilter">Cerrar búsqueda avanzada</a>-->
                    </div>
                </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<script>
    function changepage(page){
        $("#page").val(page);
        $("#filterPropertyForm").submit();
    }
    
    $(document).ready(function(){
        $(".order").change(function(){
            $("#order_by").val($(this).val());
            $("#filterPropertyForm").submit();
        });
    });
</script>