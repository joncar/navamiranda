<div class="topUserWraper">
    <a href="#" class="userNavHandler">
        <span class="icon-user"></span>
    </a>
    <div class="user-nav">
        <ul>
            <?php if(empty($_SESSION['user'])): ?>
            <li><a href="<?= base_url('panel') ?>">Conectate</a></li>
            <li><a href="<?= base_url('panel') ?>">Registrate</a></li>
            <?php else: ?>
            <li><a href="<?= base_url('panel') ?>">Entrar</a></li>
            <?php endif ?>
        </ul>
    </div>
</div>

<?php $this->load->view('includes/fragmentos/modals'); ?>
<a href="javascript:void(0);" class="top-navHandler visible-xs">
    <span class="fa fa-bars"></span>
</a>
<div class="top-nav">
    <div class="menu-top-menu-container">
        <ul class="menu">
            <li>
                <a href="<?= site_url() ?>">Inicio</a>
            </li>
            <li><a href="#">Empresa</a></li>
            <li><a href="#">Gama</a></li>
            <li><a href="#">Productos</a></li>
            <li><a href="#">Destinado a</a></li>
            <li><a href="#">Aplicaciones</a></li>
            <li><a href="#">Contacto</a></li>
        </ul>
    </div>
</div>