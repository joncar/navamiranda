<div id="hero-container">
    <div id="slideshow">
        <div style='background-image: url(<?= base_url('img/uploads/bg-6.jpg') ?>)'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-5.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-4.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-3.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-1.jpg') ?>); display:none;'></div>        
    </div>
    <div class="slideshowShadow"></div>
    <div class="home-header menuontop" id='menu'>
        <div class="home-logo osLight">
            <a href="<?= site_url() ?>">
                <?= img('img/logo.png') ?>
            </a>
        </div>

        <?php $this->load->view('includes/fragmentos/menu'); ?>
    </div>
    <div class="home-caption">
        <h1 class="home-title">NOW IT&#039;S EASY TO FIND YOUR FUTURE HOME</h1>
        <div class="home-subtitle">WITH REALES WP - REAL ESTATE WORDPRESS THEME</div>
    </div>
    <div class="search-panel">
        <form class="form-inline" method="get" action="<?= base_url('food/lista') ?>">            
            <div class="form-group">
                <input type="search" size='50' class="form-control" id="descripcion" name="descripcion" placeholder="Introduce las palabras separadas por coma" autocomplete="off">
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Destinado a</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="destinatarios_id" value="" checked="checked"><a href="javascript:void(0);">Destinado a</a>
                    </li>
                    <?php foreach($dest->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="destinatarios_id" value="<?= $d->id ?>"><a href="javascript:void(0);"><?= $d->destinatarios_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Gama</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="gamas_id" value="" checked="checked"><a href="javascript:void(0);">Gama</a>
                    </li>
                    <?php foreach($gamas->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="gamas_id" value="<?= $d->id ?>"><a href="javascript:void(0);"><?= $d->gamas_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Aplicaciones</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="aplicaciones_id" value="" checked="checked"><a href="javascript:void(0);">Aplicaciones</a>
                    </li>
                    <?php foreach($aplicaciones->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="aplicaciones_id" value="<?= $d->id ?>"><a href="javascript:void(0);"><?= $d->aplicaciones_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>            
            <div class="form-group">
                <input type="submit" id="searchPropertySubmit" class="btn btn-green" value="Buscar">
                <a href="javascript:void(0);" class="btn btn-o btn-white pull-right visible-xs" id="advanced">Búsqueda Avanzada <span class="fa fa-angle-up"></span></a>
            </div>
        </form>
    </div>
</div>
<div class="spotlight" id="page" >
    <div class="s-title osLight">Find your new place with Reales WP</div>
    <div class="s-text osLight">Fusce risus metus, placerat in consectetur eu, porttitor a est sed sed dolor lorem cras adipiscing</div>
</div>
<div class="page-wrapper">
    <div class="page-content">

        <div id="post-2" class="post-2 page type-page status-publish hentry">
            <div class="entry-content">
                <h2 class="osLight centered">Destinado a</h2>
                <div class="row pb40">
                    <?php foreach($dest->result() as $d): ?>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 s-menu-item">
                                <a href="<?= base_url('food/lista/') ?>?destinatarios_id=<?= $d->id ?>">
                                    <span class="<?= $d->icono ?> s-icon"></span>
                                    <div class="s-content">
                                        <h2 class="centered s-main osLight"><?= $d->destinatarios_nombre ?></h2>
                                        <h3 class="s-sub osLight"><?= $d->resumen ?></h3>
                                    </div>
                                </a>
                            </div>                    
                    <?php endforeach ?>
                </div>
                <h2 class="centered osLight">Gamas</h2>
                <div class="row pb40">
                    <?php foreach($gamas->result() as $d): ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <a href="<?= base_url('food/lista/') ?>?gamas_id=<?= $d->id ?>" class="propWidget-2">
                            <div class="fig">
                                <?= img('img/gamas/'.$d->icono,'',TRUE,'class="scale" alt="" data-scale="best-fill" data-align="center"') ?>
                                <?= img('img/gamas/'.$d->icono,'',TRUE,'class="blur scale" alt="" data-scale="best-fill" data-align="center"') ?>
                                <div class="opac"></div>                                
                                <h3 class="osLight"><?= $d->gamas_nombre ?></h3>
                                <div class="address"></div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach ?>
                </div>                
                <h2 class="centered osLight">Aplicaciones</h2>
                <div class="row pb40">                    
                        <?php for($i=0;$i<4;$i++): if($i<$aplicaciones->num_rows): $d = $aplicaciones->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('food/lista/') ?>?aplicaciones_id=<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/aplicaciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->aplicaciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                    </div>
                <div class="row pb40">
                    <?php for($i=4;$i<8;$i++): if($i<$aplicaciones->num_rows): $d = $aplicaciones->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('food/lista/') ?>?aplicaciones_id=<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/aplicaciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->aplicaciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                </div>
                <div class="row pb40">
                    <?php for($i=8;$i<12;$i++): if($i<$aplicaciones->num_rows): $d = $aplicaciones->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('food/lista/') ?>?aplicaciones_id=<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/aplicaciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->aplicaciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                </div>                                
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>
<div class="row contactenos">
    <h2 class="centered osLight">Contáctenos</h2>
    <p align='center'>Si tienes alguna duda o quieres preguntar por un producto</p>
    <div class='col-xs-12 col-sm-6 col-sm-offset-3'>
        <form class="contactPageForm">
            <input type="hidden" value="n3sty.marius@gmail.com" name="company_email" id="company_email">
            <div class="row">
                <div id="cp_response" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="cp_name">Name <span class="text-red">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter your name" name="cp_name" id="cp_name">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="cp_email">Email <span class="text-red">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter your email" name="cp_email" id="cp_email">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="cp_message">Message <span class="text-red">*</span></label>
                        <textarea class="form-control" rows="3" placeholder="Type your message" name="cp_message" id="cp_message"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <a id="sendContactMessageBtn" class="btn btn-green" href="javascript:void(0);">Send Message</a>
                    </div>
                </div>
            </div>
            <input type="hidden" value="e6a644bd16" name="securityContactPage" id="securityContactPage">
            <input type="hidden" value="/themes/reales-wp/contact-us/" name="_wp_http_referer">
        </form>
    </div>
</div>
<div class='page-wrapper'>
    <h2 class="centered osLight">Opiniones</h2><div class="row pb40"><div id="home-testimonials" class="carousel slide carousel-wb mb20" data-ride="carousel"><ol class="carousel-indicators"><li data-target="#home-testimonials" data-slide-to="0"class="active" ></li><li data-target="#home-testimonials" data-slide-to="1" ></li></ol><div class="carousel-inner"><div class="item active"><img src="http://mariusn.com/themes/reales-wp/wp-content/uploads/2014/12/avatar-2.png" class="home-testim-avatar" alt="Jane Smith"><div class="home-testim"><div class="home-testim-text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words</div><div class="home-testim-name">Jane Smith</div></div></div><div class="item"><img src="http://mariusn.com/themes/reales-wp/wp-content/uploads/2014/12/avatar-3.png" class="home-testim-avatar" alt="Rust Cohle"><div class="home-testim"><div class="home-testim-text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words</div><div class="home-testim-name">Rust Cohle</div></div></div></div></div></div>
</div>
<?= $this->load->view('includes/footer') ?>
<script>
    $(document).ready(function(){
        $(window).on('scroll',function(){
            if($(this).scrollTop()>$("#page").position().top-50)
                $("#menu").addClass('menuonbottom').removeClass('menuontop');
            else
                $("#menu").addClass('menuontop').removeClass('menuonbottom');
        });
        
        $("a[href*=#]").click(function(e){
            e.preventDefault();
            var target = $(this).attr('href');
            target = target.split("#");
            target = $("#"+target[1]);
            
            $('html, body').stop().animate({'scrollTop': parseInt(target.offset().top)-30}, 900, 'swing');        
    })
    });
</script>