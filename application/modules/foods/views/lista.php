<?php $this->load->view('includes/headerMain'); ?>
<section class="container-fluid" id="empresa">
    <h1>Lista de productos</h1>
    <div class="col-xs-12 col-sm-3">       
        <?php $this->load->view('includes/searchbox',array('gamas'=>$gamas)) ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="row">
             <?php foreach($lista->result() as $l): ?>
                <div class="col-sm-3 col-xs-6">
                    <a href="<?= site_url('productos/'.$l->id.'-'.toURL($l->foods_nombre)) ?>" style="text-decoration:none">
                        <div class="thumbnail">
                          <div class="img" style="background:url(<?= base_url('img/foods/'.$l->portada) ?>) no-repeat; background-size:cover; width:100%; height:160px;"></div>
                          <div class="caption">
                              <h3 class="title"><?= $l->foods_nombre ?></h3>
                            <p style="font-size:12px; overflow:hidden; text-overflow:ellipsis; height:68px;"><?= substr(strip_tags($l->descripcion),0,100).'...' ?></p>                            
                          </div>
                        </div>
                     </a>
                </div>
            <?php endforeach ?>
          </div>
        <?php if($lista->num_rows==0): ?>
            No se encontraron productos para sus criterios de búsqueda.
        <?php endif ?>
    </div>
    <div class="pull-right">
        <?php $this->load->view('predesign/paginacion',array('rows'=>'12','pages'=>6,'actual'=>$page,'total'=>$total_results,'url'=>'javascript:changepage({{page}})')); ?>
    </div>
    <div class="pull-right search_prop_calc">
        <?= (($page-1)*12)==0?1:($page-1)*12 ?> - <?= (($page-1)*12)+12 ?> de <?= $total_results ?> productos
    </div>
</section>
<script>$('html, body').stop().animate({'scrollTop': parseInt($("#empresa").offset().top)-30}, 0, 'swing'); </script>