<?php $this->load->view('includes/headerMain'); ?>
<section class="container-fluid" id="empresa">
    <h1><?= $detail->foods_nombre ?> - <?= $detail->gamas_nombre ?></h1>
    <div class="col-xs-12 col-sm-3">       
        <?php $this->load->view('includes/searchbox',array('gamas'=>$gamas)) ?>
    </div>
     <div class="col-xs-12 col-sm-9">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active" style="background:url(<?= base_url().'/img/foods/'.$detail->portada ?>) no-repeat; background-position:center;  width:100%; height:300px;"></div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
        </div>
        <?= $detail->descripcion ?>        
    </div>
</section>
<script>$('html, body').stop().animate({'scrollTop': parseInt($("#empresa").offset().top)-30}, 0, 'swing'); </script>