<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function gamas($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->set_subject('Categorias');
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','us'=>'Inglés'));
            $crud->display_as('gamas_nombre','Nombre');
            $crud->set_field_upload('icono','img/gamas');
            $crud->required_fields('gamas_nombre','icono');
            $crud->field_type('idioma','hidden','es');
            $output = $crud->render();
            $output->title = 'Categorias';
            $this->loadView($output);
        }
        function aplicaciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','us'=>'Inglés'));
            $crud->set_field_upload('icono','img/aplicaciones');
            $output = $crud->render();
            $this->loadView($output);
        }
        function destinatarios($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','us'=>'Inglés'));            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        public function fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('fotos')                        
                ->set_image_path('img/foods')
                ->set_relation_field('foods_id')
                ->set_ordering_field('priority')
                ->set_url_field('foto')
                ->module = 'foods';
            $this->loadView($crud->render());
        }
        
        function foods($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->set_subject('productos');
            //$crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','us'=>'Inglés'));
            $crud->field_type('idioma','hidden','es');
            $crud->set_field_upload('portada','img/foods');
            //$crud->set_relation_n_n('destinatarios','foods_destinatarios','destinatarios','foods_id','destinatarios_id','destinatarios_nombre','priority');
            //$crud->set_relation_n_n('aplicaciones','foods_aplicaciones','aplicaciones','foods_id','aplicaciones_id','aplicaciones_nombre','priority');
            $crud->display_as('foods_nombre','Nombre')
                     ->display_as('gamas_id','Categoria');
            $crud->columns('portada','gamas_id','foods_nombre');
            $crud->add_action('<i class="fa fa-image"></i> Adm. Fotos','',base_url('foods/admin/fotos').'/');
            $crud->required_fields('portada','gamas_id','foods_nombre','descripcion');
            $output = $crud->render();
            $output->title = 'Productos';
            $this->loadView($output);
        }
    }
?>
