<?php 
    require_once APPPATH.'/controllers/main.php';    
    class Front extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function index($x = '',$y = ''){
            $this->loadView(array(
                'view'=>'main',
                'dest'=>$this->destinatarios,
                'gamas'=>$this->gamas,
                'aplicaciones'=>$this->aplicaciones,
                'title'=>'Foods'
             ));
        }
        
        function lista(){
            
            if(empty($_GET['page'])){
                $_GET['page'] = 1;
            }
            
            $lista = new Bdsource();
            $lista->select = 'foods.*';
            $limit = ($_GET['page']-1)*12;
            $limit = $limit = 0?1:$limit;            
            $lista->limit = array('12',$limit);                        
            $lista->filters = array('destinatarios_id','gamas_id','aplicaciones_id');
            if(!empty($_GET['descripcion'])){
                $lista->like('foods_nombre',$_GET['descripcion']);
            }
            $lista->init('foods');
            $total_result = clone $lista;
            $total_result->limit  = array();
            $total_result->init('foods');
            $gamas = new Bdsource('gamas',TRUE);
            $this->loadView(array(
                'view'=>'lista',
                'title'=>'Productos',
                'lista'=>$lista,
                'gamas'=>$gamas,
                'page'=>$_GET['page'],
                'total_results'=>$total_result->num_rows
             ));
        }
        
        function read($id){
            if(empty($id)){
                throw new Exception('Producto no encontrado',404);
            }
            else{
                $id = explode("-",$id);
                $id = $id[0];
                if(is_numeric($id)){
                $detail = new Bdsource('foods');
                $detail->where('foods.id',$id);
                $detail->innerjoin('gamas');
                $detail->init();
                
                $aplicaciones = new Bdsource('foods_aplicaciones');
                $aplicaciones->where('foods_id',$id);
                $aplicaciones->leftjoin('aplicaciones');
                $aplicaciones->init();
                
                $destinatarios = new Bdsource('foods_destinatarios');
                $destinatarios->join = array('destinatarios');
                $destinatarios->where('foods_id',$id);
                $destinatarios->init();
                $gamas = new Bdsource('gamas',TRUE);
                    $this->loadView(array(
                        'view'=>'read',
                        'detail'=>$detail,
                        'destinatarios'=>$destinatarios,
                        'aplicaciones'=>$aplicaciones,
                        'title'=>$detail->foods_nombre,
                        'gamas'=>$gamas
                    ));
                }else{
                    throw new Exception('Producto no encontrado',404);
                }
            }
        }
    }
?>
