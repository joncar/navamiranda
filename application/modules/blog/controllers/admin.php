<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function blog(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/blog');
            $crud->field_type('status','true_false',array('0'=>'Borrador','1'=>'Publicado'));
            $crud->required_fields('foto','titulo','texto','fecha','status');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function clonarEntrada($id){
            if(is_numeric($id)){
                $entry = new Bdsource();
                $entry->where('id',$id);
                $entry->init('blog',TRUE,'entrada');
                $data = $this->entrada;
                $entry->save($data,null,TRUE);
                header("Location:".base_url('blog/admin/blog/edit/'.$entry->getid()));
            }
        }
    }
?>
