<?php

class Traduccion{
    public $es = array();
    public $ca = array();

    public function traducir($view,$idioma = ''){
        if(!empty($idioma)){
            foreach($this->$idioma as $n=>$v){
                $view = str_replace($n,$v,$view);
            }
            return $view;
        }else{
            return $view;
        }
    }
}    
